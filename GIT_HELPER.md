# GIT HELPER

### To creatye command "git nlog"
EN: Lists 10 most recent commits without much description.
PT: Lista 10 entregas mais recentes sem muitas descrições.

$ git config --local alias.nlog "log --oneline --decorate --all --graph -n 10"
$ git config --local alias.nl "log --oneline --decorate --all --graph -n 10"

### To creatye command "git ncommit"
EN: Add all files to stack and commit with empty message.
PT: Adicione todos os arquivos para o receptáculo e entrega com mensagem vazia.

git config --local alias.nc 'commit -a --allow-empty-message -m ""'
git config --local alias.ncommit 'commit -a --allow-empty-message -m ""'
