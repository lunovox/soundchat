![screenshot]

# 🗣️ [SOUNDCHAT][download_repository] 📢

Badge: [![ContentDB_en_icon]][ContentDB_en_link] [![DownloadsCDB_en_icon]][DownloadsCDB_en_link]

Chat interface with [audible beep] of incoming messages. Chat individually configurable for Local, Global, Private modes by the player himself. In addition, each player can create their own list of ignored players. It also adds colored nametags according to each player's privileges.


## **License:**

* [![license_code_icon]][license_code_link]
* [![license_media_icon]][license_media_link]

See more details in wiki: 
 
[![wiki_en_icon]][wiki_en_link] [![wiki_pt_icon]][wiki_pt_link]


## **Depends:**

* [e-urn][mod_eurn](Optional) : Displays the colored nametag of the president elected by the server's players.
* [i3][mod_i3] (Optional) : Integrate at menu of mod i3.
* [intllib][mod_intllib] (Optional) : Facilitates the translation of several other mods into your native language, or other languages.
* [lib_savelogs][mod_lib_savelogs] (Optional) : Save dialog chats.
* [sfinv][mod_sfinv] (Optional) : Integrate at menu of mod default of Minetest Game
* [unified_inventory][mod_unified_inventory] (Optional) : Integrate at menu of mod unified_inventory.
* [computing][mod_computing](Optional) : Displays control of 'chat sound' and 'private chat' via computing device (items: Smartphone or Desktop PC).

## **Developers:**

* Lunovox Heavenfinder: [email](mailto:lunovox@disroot.org), [xmpp](xmpp:lunovox@disroot.org?join), [social web](http:mastodon.social/@lunovox), [audio conference](mumble:libreplanetbr.org), [more contacts](https://libreplanet.org/wiki/User:Lunovox)

## **Downloads:**

* [Stable Versions][download_stable] : There are several versions in different stages of completion.  Usually recommended for servers because they are more secure.

* [Git Repository Unstable][download_repository] : It is the version currently under construction.  There may be some stability if used on servers.  It is only recommended for testers and mod developers.


## **Privileges:**

These privileges are not mandatory for the mod to work. But, they help the admin to have some unique privileges.

* ````superear```` : Allows you to read all messages sent by the player in real time, no matter how far away.
* ````globalchat```` : Allows you to enable chat in GLOBAL mode by command, to send message to all players, regardless of distance.

## **Commands:**

* ````/soundchat```` or ````/schat```` or ````/sc```` : Show the settings graphic panel of this mod. (See the [screenshot]!)
* ````/localchat```` or ````/lchat```` or ````/lc```` to enable chat in LOCAL mode. Only players within the configured distance of <soundchat.max_dialog_dist> will be able to read the sent chat message.
* ````/globalchat```` or ````/gchat```` or ````/gc```` to enable chat in GLOBAL mode. (Need the priv 'globalchat')
* ````/privatechat <playername>```` or ````/pc <playername>```` to enable chat in PRIVATE mode with a specific player, no matter the distance.
* ````/ignore <playername>```` or ````/ign <playername>```` to Add or to Remove a player from your chat ignore list. (Do not add admin player).
* ````/mute```` or ````/mudo```` to enables and disables the simple sound of your own individual chat.
* ````/mutecall```` or ````/nomemudo```` or ````/emudecerchamada```` to enables and disables the alarm sound of call your name.
* ````/alert <message>```` to send a colored warning with flashy sound for all players (Need the priv 'server')
* ````/smartshutdown```` or ````/sshutdown```` or ````/ssd```` to shutdown the server after a public message and a alert sound.

## **Settings:**

### In **minetest.conf** file:

You don't need to do any of these presets mentioned below to make this mod work. But it's worth knowing the configuration values ​​in case you want to directly change the ````minetest.conf```` file.

* ````soundchat = <boolean>```` : Enables all sounds of mod soundchat. Default: ````true````
* ````soundchat.call.onsendmessage = <boolean>```` : Enables sound in chat when the player sends a message to all players. Default: ````true````
* ````soundchat.call.onplayername = <boolean>```` : Play a alarm sound when send the player name in chat. Default: ````true````
* ````soundchat.terminaldialogs = <boolean>```` : Print in shell terminal (out of game) all sended player dialogs. Default: ````true````
* ````soundchat.max_dialog_dist = <integer>```` : Maximum distance someone has heard your typed message. Default Distance = ````16```` | Infinite Distance (No distance limit) = ````0```` | Maximum Distance = ````30000````
* ````soundchat.color.player = <colorcode>```` : Print the player name colored in terminal dialogs. Default: ````#00FF00````
* ````soundchat.color.admin = <colorcode>```` : Print the admin name colored in terminal dialogs. Default: ````#FF0000````
* ````soundchat.debug = <boolean>```` : Allows you to print the debug information of this mod on the screen. Default: ````false````

### In **[nametags.lua]** file:

You don't need to do any of these presets mentioned below to make this mod work. But it's worth knowing the configuration values ​​in case you want Add new name tags to the chat.

```lua
["<Player Type>"] = {-- Nametags in chat
	priv = "<privilege>", --One word in lower case
	color = "<collor code>", --Yellow Collor Code
	description = "<A short description of player type.>",
},
```

Example:

```lua
["Peertuber"] = {-- Nametags in chat
	priv = "peertuber", --One word in lower case
	color = "#FFFF00", --Yellow Collor Code
	description = "Publish videos on the PeerTube platform",
},
```
Finally, just enter the game in single player mode and write the command:

`/grantme peertuber`

Now you will notice that your name tag has changed. ☺️

## **Internationalization of this Mod:**

This mod currently are configured to language:

* [English][lang_en_template] (Default/Template)
* [Portuguese (Generic)][lang_pt]

To add a new language to this mod just follow the steps below:

1. Enable the complementary mod **'intllib'.**
2. Set your language in **'minetest.conf'** by adding the [````language = <your language>````] property.
3. Example for French Language: ````language = fr````
4. Make a copy of the file [ **template.pot** ] in the [ **locale** ] folder that is inside the mod for [````locale/<your_language>.po````]. 
5. Example for French language: ````locale/fr.po````
6. Open the file [````locale/<your_language>.po````] in POEdit (Also works in a simple text editor).
7. Translate all and send your translated file to developers of this mod.


--------------------------------------------

[audible beep]:https://gitlab.com/lunovox/soundchat/-/raw/master/sounds/sfx_chat_speak.ogg
[favicon]:https://gitlab.com/lunovox/soundchat/-/raw/master/favicon.png
[nametags.lua]:https://gitlab.com/lunovox/soundchat/-/raw/master/nametags.lua
[screenshot]:https://gitlab.com/lunovox/soundchat/-/raw/master/screenshot.png
[site_minetest]:https://minetest.net


[ContentDB_en_icon]:https://content.luanti.org/packages/Lunovox/soundchat/shields/title/
[ContentDB_en_link]:https://content.luanti.org/packages/Lunovox/soundchat/
[DownloadsCDB_en_icon]:https://content.luanti.org/packages/Lunovox/soundchat/shields/downloads/
[DownloadsCDB_en_link]:https://gitlab.com/lunovox/soundchat/-/tags

[download_stable]:https://gitlab.com/lunovox/soundchat/-/tags
[download_repository]:https://gitlab.com/lunovox/soundchat
[download_content]:https://content.luanti.org/packages/lunovox/soundchat/

[lang_en_template]:https://gitlab.com/lunovox/soundchat/-/raw/master/locale/template.pot
[lang_pt]:https://gitlab.com/lunovox/soundchat/-/raw/master/locale/pt.po

[mod_computing]:https://gitlab.com/lunovox/computing
[mod_eurn]:https://gitlab.com/lunovox/e-urn
[mod_intllib]:https://github.com/minetest-mods/intllib
[mod_i3]:https://content.luanti.org/packages/jp/i3/
[mod_lib_savelogs]:https://gitlab.com/lunovox/lib_savelogs
[mod_sfinv]:https://content.luanti.org/packages/rubenwardy/sfinv/
[mod_unified_inventory]:https://content.luanti.org/packages/RealBadAngel/unified_inventory/

[license_code_icon]:https://img.shields.io/static/v1?label=LICENSE%20CODE&message=GNU%20AGPL%20v3.0&color=yellow
[license_code_link]:https://gitlab.com/lunovox/soundchat/-/raw/master/LICENSE_CODE
[license_media_icon]:https://img.shields.io/static/v1?label=LICENSE%20MEDIA&message=CC%20BY-SA-4.0&color=yellow
[license_media_link]:https://gitlab.com/lunovox/soundchat/-/raw/master/LICENSE_MEDIA
[wiki_en_icon]:https://img.shields.io/static/v1?label=GNU%20AGPL%20v3.0&message=EN&color=blue
[wiki_en_link]:https://en.wikipedia.org/wiki/GNU_Affero_General_Public_License
[wiki_pt_icon]:https://img.shields.io/static/v1?label=GNU%20AGPL%20v3.0&message=PT&color=blue
[wiki_pt_link]:https://pt.wikipedia.org/wiki/GNU_Affero_General_Public_License
