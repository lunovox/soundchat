
modSoundChat.doSaveLog = function(text_log, isSave)
	if minetest.global_exists("libSaveLogs")
	   --minetest.get_modpath("lib_savelogs") 
	   --and libSaveLogs~=nil 
	then
		libSaveLogs.addLog(text_log)
		if isSave then
			libSaveLogs.doSave()
		end
	end
end

modSoundChat.debug = function(text, playername)
	if text ~= nil
		and type(text) == "string"
		and text ~= ""
	then
		if minetest.settings:get_bool("soundchat.debug") then
			if playername ~= nil
				and type(playername) == "string"
				and playername ~= ""
			then
				local player = minetest.get_player_by_name(playername)
				if player ~=nil and player:is_player() then
					minetest.chat_send_player(
						playername, text
					)
				else
					minetest.log('error',
						"["..modSoundChat.modname..":DEBUG] "
						..modSoundChat.translate(
							"Unable to address debug for player '%s'."
						):format(dump(playername))
					)
				end
			else
				minetest.chat_send_all(text)
			end
		end
	end
end

modSoundChat.dbSave = function()
   local file = io.open(modSoundChat.database_file, "w")
	if file then
		local content = minetest.serialize(modSoundChat.players)
		content = minetest.encode_base64(content)
		file:write(content)
		file:close()
		minetest.log('action',"[SOUNDCHAT] "..modSoundChat.translate("Saving database in the file '%s'!"):format(modSoundChat.database_file))
	else
		minetest.log('error',"[SOUNDCHAT:ERRO] "..modSoundChat.translate("The file '%s' is not in table format!"):format(modSoundChat.database_file))
	end
end

modSoundChat.dbLoad = function()
   local file = io.open(modSoundChat.database_file, "r")
	if file then
	   local content = minetest.decode_base64(file:read("*all"))
	   modSoundChat.players = minetest.deserialize(content)
		file:close()
		if type(modSoundChat.players) ~= "table" then
			modSoundChat.players = { }
			minetest.log('warning',"[SOUNDCHAT:WARNING] "..modSoundChat.translate("The file '%s' is not in table format!"):format(modSoundChat.database_file))
		else
			minetest.log('action',"[SOUNDCHAT] "..modSoundChat.translate("Opening '%s' with database!"):format(modSoundChat.database_file))
		end
	end
end

modSoundChat.openConfNametags = function()
	modSoundChat.count_nametags = 0
	for tag_name,tag_def in pairs(modSoundChat.ChatTags) do
		--Faltou verifucar se o privilégio já estava previamente registrado.
		--if minetest.get_player_privs(playername)[tag_def.priv] then
		local newDescription = modSoundChat.translate(tag_def.description) or ""
		minetest.register_privilege(tag_def.priv,  {
			description=newDescription, 
			give_to_singleplayer=false,
		})
		minetest.log('action',
			"["..modSoundChat.modname:upper().."] "..
			modSoundChat.translate("Registering player type '%s' with privilege '%s'...!"):format(tag_name, tag_def.priv).."\n"
			.."   * "..newDescription
		)
		modSoundChat.count_nametags = modSoundChat.count_nametags + 1
	end
end; modSoundChat.openConfNametags()

modSoundChat.doNormalizeNametag = function(playername)
	local player = minetest.get_player_by_name(playername)
	if player ~=nil and player:is_player() then
		if minetest.get_player_privs(playername).server then
			player:set_nametag_attributes({
				color = modSoundChat.getColorNameAdmin()
			})
		else
			local sent = false
			if modSoundChat.count_nametags >= 1 then
				for tag_name,tag_def in pairs(modSoundChat.ChatTags) do
					if 
						tag_def.priv ~= nil 
						and type(tag_def.priv)=="string" 
						and tag_def.priv ~= "" 
						and minetest.get_player_privs(playername)[tag_def.priv] 
					then
						sent = true
						player:set_nametag_attributes({
							color = tag_def.color
						})
						break
					end
				end
			end
			if sent == false then
				player:set_nametag_attributes({
					--color = modSoundChat.getColorNamePlayer()
					color = "#FFFFFF"
				})
			end
		end
	end
end

modSoundChat.doSoundPlayer = function(playername, sfxFile, intDistace)
	if intDistace == nil or (type(intDistace) == "number" and intDistace>= 0) then
		if type(playername) == "string" and type(sfxFile) == "string" and playername ~= "" and sfxFile ~= "" then
			local player = minetest.get_player_by_name(playername)
			if player ~=nil and player:is_player() then
				return minetest.sound_play(sfxFile, {
					object = player, --Se retirar esta linha tocará para todos. (Provavelmente ¬¬)
					gain = 1.0, -- 1.0 = Volume total
					max_hear_distance = intDistace,
					loop = false,
				})
			end
		end
	end
end

modSoundChat.isEnabled = function()
   local conf = minetest.settings:get("soundchat")
   if type(conf)=="nil" or conf == "" then
         conf = true
      minetest.settings:set_bool("soundchat", conf)
   end
   return conf
end

modSoundChat.isPrintTerminalDialogs = function()
   local conf = minetest.settings:get("soundchat.terminaldialogs")
   if type(conf)=="nil" or conf == "" then
      conf = true
      minetest.settings:set_bool("soundchat.terminaldialogs", conf)
   end
   return conf
end

modSoundChat.isSaveDialogs = function()
   local conf = minetest.settings:get("soundchat.savedialogs")
   if type(conf)=="nil" or conf == "" then
      conf = true
      minetest.settings:set_bool("soundchat.savedialogs", conf)
   end
   return conf
end

modSoundChat.getChatMode = function(playername)
	if playername ~= nil and type(playername) == "string" and playername ~= "" then
		--[[
		if not modSoundChat.players then
			modSoundChat.players = { }
		end
		--]]
		if not modSoundChat.players[playername] then 
			modSoundChat.players[playername] = { }
		end
		if modSoundChat.players[playername].chatmode == nil or modSoundChat.players[playername].chatmode == "" then 
			modSoundChat.players[playername].chatmode = "local"
		end
		return modSoundChat.players[playername].chatmode
	end
end

modSoundChat.setChatMode = function(playername_from, mode, playername_to)
	if not modSoundChat.players[playername_from] then 
		modSoundChat.players[playername_from] = { }
	end
	if mode ~= nil and mode == "local" then
		modSoundChat.players[playername_from].chatmode = "local"
		local msg = "Starting LOCAL chat mode!"
		modSoundChat.doSaveLog("[SOUNDCHAT:"..playername_from.."] "..msg, false)
		minetest.chat_send_player(playername_from, 
			--core.get_color_escape_sequence("#ffff00")..sendername..core.get_color_escape_sequence("#00ffff").." citou seu nome!"
			core.colorize("#00FF00", "[SOUNDCHAT] ")
			..core.colorize("#888888", modSoundChat.translate(msg))
		)
		--modSoundChat.doSoundPlayer(playername_from, "sfx_chat2")
		return "local"
	elseif mode ~= nil and mode == "global" then
		if minetest.get_player_privs(playername_from).globalchat then
			modSoundChat.players[playername_from].chatmode = "global"
			local msg = "Starting GLOBAL chat mode!"
			modSoundChat.doSaveLog("[SOUNDCHAT:"..playername_from.."] "..msg, false)
			minetest.chat_send_player(playername_from, 
				--core.get_color_escape_sequence("#ffff00")..sendername..core.get_color_escape_sequence("#00ffff").." citou seu nome!"
				core.colorize("#00FF00", "[SOUNDCHAT] ")
				..core.colorize("#888888", modSoundChat.translate(msg))
			)
			--modSoundChat.doSoundPlayer(playername_from, "sfx_chat2")
			return "global"
		else
			modSoundChat.players[playername_from].chatmode = "local"
			local msg = "You do not have the 'globalchat' privilege to enter GLOBAL chat mode."
				.." "..modSoundChat.translate("Forcefully entering 'LOCAL' chat mode...")
			
			modSoundChat.doSaveLog("[SOUNDCHAT:ALERT:"..playername_from.."] "..msg, false)
			minetest.chat_send_player(playername_from, 
				--core.get_color_escape_sequence("#ffff00")..sendername..core.get_color_escape_sequence("#00ffff").." citou seu nome!"
				core.colorize("#FFFF00", "[SOUNDCHAT:ALERT] ")
				..core.colorize("#888888", modSoundChat.translate(msg))
			)
			--modSoundChat.doSoundPlayer(playername_from, "sfx_error")
			return nil
		end
	elseif mode ~= nil and mode == "private" and playername_to ~= nil and playername_to ~= "" then
		if playername_from ~= playername_to then
			local isPrivate = modSoundChat.setPvtName(playername_from, playername_to)
			if isPrivate then
				local msgstriped = ("Starting PRIVATE chat mode with player: %s"):format(playername_to)
				local msg = modSoundChat.translate("Starting PRIVATE chat mode with player: %s"):format(
               core.colorize("#00FF00", playername_to)
				)
				modSoundChat.doSaveLog("[SOUNDCHAT:"..playername_from.."] "..msgstriped, false)
				minetest.chat_send_player(playername_from, 
					--core.get_color_escape_sequence("#ffff00")..sendername..core.get_color_escape_sequence("#00ffff").." citou seu nome!"
					core.colorize("#00FF00", "[SOUNDCHAT] ")
					..core.colorize("#888888", msg)
				)
				--modSoundChat.doSoundPlayer(playername_from, "sfx_chat2")
				return "private"
			else
				minetest.chat_send_player(playername_from, 
					--core.get_color_escape_sequence("#ffff00")..sendername..core.get_color_escape_sequence("#00ffff").." citou seu nome!"
					core.colorize("#FFFF00", "[SOUNDCHAT:ALERT] ")
					..core.colorize("#888888", 
						modSoundChat.translate("Cannot enter PRIVATE chat mode with absent player named: '%s'."):format(playername_to).." "
						..modSoundChat.translate("Forcefully entering 'LOCAL' chat mode...")
					)
				)
				--modSoundChat.doSoundPlayer(playername_from, "sfx_error")
				return nil
			end
		else
			minetest.chat_send_player(playername_from, 
				--core.get_color_escape_sequence("#ffff00")..sendername..core.get_color_escape_sequence("#00ffff").." citou seu nome!"
				core.colorize("#FFFF00", "[SOUNDCHAT:ALERT] ")
				..core.colorize("#888888", 
					modSoundChat.translate("It is not possible to enter PRIVATE mode of chat with yourself.").." "
					..modSoundChat.translate("Forcefully entering 'LOCAL' chat mode...")
				)
			)
			--modSoundChat.doSoundPlayer(playername_from, "sfx_error")
			return nil
		end
	else
		modSoundChat.players[playername_from].chatmode = "local"
		minetest.chat_send_player(playername_from, 
			--core.get_color_escape_sequence("#ffff00")..sendername..core.get_color_escape_sequence("#00ffff").." citou seu nome!"
			core.colorize("#FF0000", "[SOUNDCHAT:ERRO] ")
			..modSoundChat.translate("Invalid chat mode setting '%s'!"):format(core.colorize("#FF0000", dump(mode)))
		)
		--print(("Configuração de modo de char '%s' inválido!"):format(dump(mode)))
		--modSoundChat.doSoundPlayer(playername_from, "sfx_error")
		return nil
	end
end

modSoundChat.setPvtName = function(playername, privatename)
	if not modSoundChat.players[playername] then 
		modSoundChat.players[playername] = { }
	end
	local playerprivate = minetest.get_player_by_name(privatename)
	if privatename ~= nil 
		and type(privatename) == "string" 
		and privatename ~= ""
		and playerprivate ~=nil 
		and playerprivate:is_player() 
	then
		modSoundChat.players[playername].chatmode = "private"
		modSoundChat.players[playername].private_playername = privatename
		return true
	else
		modSoundChat.players[playername].chatmode = "local"
		modSoundChat.players[playername].private_playername = ""
		return false
	end
end


modSoundChat.getPvtName = function(playername)
	if not modSoundChat.players[playername] then 
		modSoundChat.players[playername] = { }
	end
	if modSoundChat.players[playername].chatmode == "private" 
		and modSoundChat.players[playername].private_playername ~= nil 
		and type(modSoundChat.players[playername].private_playername) == "string"
		and modSoundChat.players[playername].private_playername ~= ""
	then
		return modSoundChat.players[playername].private_playername 
	end
	return ""
end


modSoundChat.getMaxDialogDist = function()
   local conf = minetest.settings:get("soundchat.max_dialog_dist")
   if type(conf)=="nil" or conf == "" then
      conf = "48"
      core.settings:set("soundchat.max_dialog_dist", conf)
   end
   return tonumber(conf) or 48
end

modSoundChat.getColorNamePlayer = function()
   local conf = minetest.settings:get("soundchat.color.player")
   if type(conf)=="nil" or conf == "" then
      conf = "#00FF00"
      core.settings:set("soundchat.color.player", conf)
   end
   return conf
end

modSoundChat.getColorNameAdmin = function()
   local conf = minetest.settings:get("soundchat.color.admin")
   if type(conf)=="nil" or conf == "" then
      conf = "#FF0000"
      core.settings:set("soundchat.color.admin", conf)
   end
   return conf
end

modSoundChat.isCall = {
   SendMessage = function()
      local conf = minetest.settings:get("soundchat.call.onsendmessage")
      if type(conf)=="nil" or conf == "" then
         conf = true
         core.settings:set_bool("soundchat.call.onsendmessage", conf)
      end
      return conf
   end,
   PlayerName = function()
      local conf = minetest.settings:get("soundchat.call.onplayername")
      if type(conf)=="nil" or conf == "" then
         conf = true
         core.settings:set_bool("soundchat.call.onplayername", conf)
      end
      return conf
   end,
}

--[[
if core.settings:get_bool("disable_escape_sequences")~= false then
	core.settings:set_bool("disable_escape_sequences", true)
	--core.colorize(color, message)
	--core.get_background_escape_sequence("#00ff00")
	--core.get_color_escape_sequence("#ff0000")
end
--]]

modSoundChat.doAlert = function(sendername, msg)
	if type(msg)=="string" and msg~="" then
		local players = minetest.get_connected_players()
		for _, player in pairs(players) do
			if player~=nil and player:is_player() then
				minetest.sound_play("sfx_alertfire", {object=player})
				local playername = player:get_player_name()
			
				local form = "\n"
					..core.get_color_escape_sequence("#ff0000").."#######################################################################################\n"
					..core.get_color_escape_sequence("#ff0000").."###    "..core.get_color_escape_sequence("#00ff00")..modSoundChat.translate("ADMINISTRATOR NOTICE")..":\n"
					..core.get_color_escape_sequence("#ff0000").."###         "..core.get_color_escape_sequence("#ffffFF").." -> "..msg.."\n"
					..core.get_color_escape_sequence("#ff0000").."#######################################################################################\n"
				minetest.chat_send_player(playername, form)
				minetest.log('action',form)
			end
		end
		return true, ""
	else
		minetest.chat_send_player(sendername, 
			core.colorize("#00FF00", "[SOUNDCHAT:ERRO] ")
			..core.colorize("#8888FF", "/alert <"..modSoundChat.translate("message").."> ")
		, false)
	end
	return false
end

modSoundChat.doTimerShutdown = function(secounds, message)
   if type(secounds)=="number" then
      if secounds>=0 then
         modSoundChat.timeCounter = secounds
      else
         avisodeerror()
      end
   else
      if type(modSoundChat.timeCounter)~="number" or modSoundChat.timeCounter<0 then
         modSoundChat.timeCounter = 0
      else
         modSoundChat.timeCounter = modSoundChat.timeCounter - 1
      end
   end
   if type(message)=="string" and message~="" then
      modSoundChat.textShutdown = message
   end
   if modSoundChat.timeCounter < 1 then 
      if type(modSoundChat.textShutdown)=="string" and modSoundChat.textShutdown~="" then
         minetest.chat_send_all(core.colorize("#ff0000", modSoundChat.textShutdown))
      end
      minetest.request_shutdown()
   elseif modSoundChat.timeCounter % 5 == 0 and type(modSoundChat.textShutdown)=="string" and modSoundChat.textShutdown~="" then
      minetest.chat_send_all((("Reiniciando em: %02d"):format(modSoundChat.timeCounter)).." ("..core.colorize("#ff0000", modSoundChat.textShutdown)..")")
      minetest.after(1.0, modSoundChat.doTimerShutdown)
   else
      minetest.chat_send_all(("Reiniciando em: %02d"):format(modSoundChat.timeCounter))
      minetest.after(1.0, modSoundChat.doTimerShutdown)
   end
end

modSoundChat.doShutdownAlert = function(sendername, params)
   local message = modSoundChat.translate("Server will be restarted. We'll be back in 5 minutes...")
   modSoundChat.doAlert(sendername, message)
   if params == nil or params == "" then
      modSoundChat.doTimerShutdown(30, message)
   elseif(params:find("%D+")) then
      modSoundChat.doTimerShutdown(30, message)
   else 
      modSoundChat.doTimerShutdown(tonumber(params), message)
   end
end

modSoundChat.doMute = function(playername)
	if type(playername)=="string" and playername~="" then
		if not modSoundChat.players[playername] then 
			modSoundChat.players[playername] = { }
		end
		modSoundChat.players[playername].mute = not (type(modSoundChat.players[playername].mute)=="boolean" and modSoundChat.players[playername].mute==true)
		if not modSoundChat.players[playername].mute then --Verifica se o chat esta ativado!
			minetest.chat_send_player(playername, 
				core.colorize("#00FF00", "[SOUNDCHAT]").." "
				..modSoundChat.translate("The chat sounder was %s!"):format(core.colorize("#00FFFF", modSoundChat.translate("ACTIVATED")))
			)
		else
			minetest.chat_send_player(playername, 
				core.colorize("#00FF00", "[SOUNDCHAT]").." "
				..modSoundChat.translate("The chat sounder was %s!"):format(core.colorize("#FF0000", modSoundChat.translate("DEACTIVATED")))
			)
		end
		--[[
		local player = minetest.get_player_by_name(playername)
		if player ~=nil and player:is_player() then
    			minetest.sound_play("sfx_chat2", {
    				object = player, --Se retirar esta linha tocará para todos. (Provavelmente ¬¬)
    				gain = 1.0, -- 1.0 = Volume total
    				--max_hear_distance = 1,
    				loop = false,
    			})
		end
		--]]
		--modSoundChat.doSoundPlayer(playername, "sfx_chat2")
		return modSoundChat.players[playername].mute
	end
end

modSoundChat.doMuteCall = function(playername)
	if type(playername)=="string" and playername~="" then
		if not modSoundChat.players[playername] then 
			modSoundChat.players[playername] = { }
		end
		modSoundChat.players[playername].muteCall = not (type(modSoundChat.players[playername].muteCall)=="boolean" and modSoundChat.players[playername].muteCall==true)
		if not modSoundChat.players[playername].muteCall then --Verifica se a chamada de nome de jogador esta ativada!
			minetest.chat_send_player(playername, 
				core.colorize("#00FF00", "[SOUNDCHAT]").." "
				..modSoundChat.translate("The Bell by player name call was %s!"):format(core.colorize("#00FFFF", modSoundChat.translate("ACTIVATED")))
			)
		else
			minetest.chat_send_player(playername, 
				core.colorize("#00FF00", "[SOUNDCHAT]").." "
				..modSoundChat.translate("The Bell by player name call was %s!"):format(core.colorize("#FF0000", modSoundChat.translate("DEACTIVATED")))
			)
		end
		--[[
		local player = minetest.get_player_by_name(playername)
		if player ~=nil and player:is_player() then
    			minetest.sound_play("sfx_chat2", {
    				object = player, --Se retirar esta linha tocará para todos. (Provavelmente ¬¬)
    				gain = 1.0, -- 1.0 = Volume total
    				--max_hear_distance = 1,
    				loop = false,
    			})
		end
		--]]
		--modSoundChat.doSoundPlayer(playername, "sfx_chat2")

		return modSoundChat.players[playername].muteCall
	end
end

modSoundChat.getDistance = function(player1, player2)
   if player1 and player1:is_player() and player2 and player2:is_player() then
		local p1 = player1:get_pos()
		local p2 = player2:get_pos()
		local dist = math.sqrt(
			((p1.x-p2.x)^2)
			+((p1.y-p2.y)^2)
			+((p1.z-p2.z)^2)
		)
		return dist
	end
end

modSoundChat.doChangeIgnored = function(playername, ignoredname)
	modSoundChat.debug()
	if type(playername)=="string" and playername~="" then
		--local ignoredplayer = minetest.get_player_by_name(ignoredname)
		if type(ignoredname) == "string" 
		   and ignoredname ~= ""
		   and minetest.player_exists(ignoredname)
		   --and ignoredplayer:is_player() 
		then
			if not minetest.get_player_privs(ignoredname).server then
				if not modSoundChat.players[playername] then 
					modSoundChat.players[playername] = { }
				end
				if not modSoundChat.players[playername].ignoreds then 
					modSoundChat.players[playername].ignoreds = { }
				end
				if type(modSoundChat.players[playername].ignoreds[ignoredname]) == "nil" then 
					modSoundChat.players[playername].ignoreds[ignoredname] = true
					minetest.chat_send_player(playername, 
						"["..core.colorize("#FFFF00", "SOUNDCHAT").."] "
						..(modSoundChat.translate("Player '%s' has been added to your ignore list!")):format(ignoredname)
					)
				else
					modSoundChat.players[playername].ignoreds[ignoredname] = nil
					minetest.chat_send_player(playername, 
						"["..core.colorize("#FFFF00", "SOUNDCHAT").."] "
						..(modSoundChat.translate("Player '%s' has been removed from your ignore list!")):format(ignoredname)
					)
				end
				modSoundChat.doSoundPlayer(playername, "sfx_chat2", 0)
				return true
			else
				minetest.chat_send_player(playername, 
					core.colorize("#FF0000", "[SOUNDCHAT:ERRO]").." "
					..modSoundChat.translate("You cannot add admin '%s' to your ignore list!"):format(ignoredname)
				)
				modSoundChat.doSoundPlayer(playername, "sfx_error", 0)
				return "serverplayer"
			end
		else
			minetest.chat_send_player(playername, 
				core.colorize("#FF0000", "[SOUNDCHAT:ERRO]").." "
				..modSoundChat.translate("The player '%s' does not exist to by ignored!"):format(ignoredname)
			)
			modSoundChat.doSoundPlayer(playername, "sfx_error", 0)
			return "offline"
		end
	end
end

modSoundChat.isIgnored = function(sendername, receivername)
	if type(sendername)=="string" and sendername:trim() ~= "" then
		if type(receivername) == "string" and receivername:trim() ~= "" then
			if not modSoundChat.players[receivername] then 
				modSoundChat.players[receivername] = { }
			end
			if not modSoundChat.players[receivername].ignoreds then 
				modSoundChat.players[receivername].ignoreds = { }
			end			
			return (type(modSoundChat.players) == "table"
				and type(modSoundChat.players[receivername]) == "table" 
				and type(modSoundChat.players[receivername].ignoreds) == "table"
				and type(modSoundChat.players[receivername].ignoreds[sendername]) == "boolean" 
				and modSoundChat.players[receivername].ignoreds[sendername] == true
			)
		else
	      return false, "receivername"
		end
	else
	   return false, "sendername"
	end
end

modSoundChat.getListIgnoreds = function(playername)
	local tblIgnoreds = { }
	if type(playername)=="string" and playername:trim() ~= "" then
		if not modSoundChat.players[playername] then 
			modSoundChat.players[playername] = { }
		end
		if not modSoundChat.players[playername].ignoreds then 
			modSoundChat.players[playername].ignoreds = { }
		end
		local dbsIgnoreds = modSoundChat.players[playername].ignoreds
		
		--minetest.chat_send_all("dbsIgnoreds = "..dump(dbsIgnoreds))
		for ignName, ignValue in pairs(dbsIgnoreds) do
			--minetest.chat_send_all("ignName = "..dump(ignName).." | ignValue = "..dump(ignName))
			table.insert(tblIgnoreds, ignName)
		end
	end
	return tblIgnoreds
end

modSoundChat.getHistoric = function(playername)
	if type(modSoundChat.players)~="table" then
		modSoundChat.players = { }
	end
	if type(modSoundChat.players[playername])~="table" then
		modSoundChat.players[playername] = { }
	end
	if type(modSoundChat.players[playername].historic)~="string" then
		modSoundChat.players[playername].historic = ""
	end
	--modSoundChat.debug("modSoundChat.getHistoric() : modSoundChat.players[playername].historic = "..dump(modSoundChat.players[playername].historic))
	return modSoundChat.players[playername].historic
end

modSoundChat.setHistoric = function(playername, historic)
	if type(playername)=="string" and playername~="" and minetest.player_exists(playername) then
		if type(historic)=="string" then
			if type(modSoundChat.players)~="table" then
				modSoundChat.players = { }
			end
			if type(modSoundChat.players[playername])~="table" then
				modSoundChat.players[playername] = { }
			end
			--modSoundChat.debug("modSoundChat.setHistoric() : historic = "..dump(historic))
			modSoundChat.players[playername].historic = historic
			return true, "OK"
		else
			return false, ("The variable '%s' does have the invalid value '%s'!"):format("historic", dump(historic))
		end
	else
		return false, ("There is no player '%s' registered on the server!"):format(dump(playername))
	end
end

modSoundChat.addHistoric = function(playername, historic)
	if type(historic)=="string" and historic ~= "" then
   	local hist = modSoundChat.getHistoric(playername)
   	if hist~="" then
   		local _, countLines = string.gsub(hist, "\n", "\n") --count line numbers.
   		if countLines > 100 then
   			local tentativas = 0
   			while (countLines > 100 and tentativas <= 100 ) do --Always keep the maximum number of lines less than or equal to 100.
   				tentativas = tentativas + 1
   				local nlPos = string.find(hist, "\n", 1, true) --Return the position to end of the first line.
   				if type(nlPos)=="number" and nlPos > 0 then
   					hist = string.sub(hist, nlPos + 1, string.len(hist)) --Removes the first line.
   				end
   				_, countLines = string.gsub(hist, "\n", "\n") --count line numbers.
   			end
   		end
   		hist = hist.."\n"
   	end
   	local when = os.date("%Y-%m-%d %Hh:%Mm:%Ss", os.time())
   	hist = hist..when.." >> "..historic
   	--modSoundChat.debug("modSoundChat.addHistoric() : hist = "..dump(hist))
   	return modSoundChat.setHistoric(playername, hist)
   else
      minetest.log('error',
			"["..modSoundChat.modname..":ERRO] "
			..("%s >>> Variable '%s' is '%s'!"):format(
			   "modSoundChat.addHistoric(playername, historic)", 
			   "historic", 
			   dump(historic)
			)
		)
	end
end

modSoundChat.propCommandShowHistoric = function()
	return { 
		params="[<playername>]", 
		privs={},
		description = modSoundChat.translate("Show the ultimate dialogs."),
		func = function(sendername, playername)
			if type(playername)~="string" or playername=="" then
				playername = sendername
			elseif not minetest.player_exists(playername) then
				local title = "[SOUNDCHAT:ALERT]"
				local msg = modSoundChat.translate("Player '@1' is not registered on this server!"):format(dump(playername))
				minetest.chat_send_player(sendername, core.colorize("#FFFF00", title).." "..core.colorize("#888888", msg))
				modSoundChat.doSoundPlayer(sendername, "sfx_error")
				return false, title.." "..msg
			elseif 
				sendername ~= playername
				and not minetest.get_player_privs(sendername).server 
				and (
					not minetest.global_exists("modEUrn") 
					or type(modEUrn.getPresidentName())~="string"  --<string/nil> modEUrn.getPresidentName()
					or modEUrn.getPresidentName() ~= sendername 
				)
			then
				local title = "[SOUNDCHAT:ALERT]"
				local msg = modSoundChat.translate("You are not authorized to read player '%s' dialogue! (requires 'server' privilege, or be elected to President of the server)"):format(dump(playername))
				minetest.chat_send_player(sendername, core.colorize("#FFFF00", title).." "..core.colorize("#888888", msg))
				modSoundChat.doSoundPlayer(sendername, "sfx_error")
				return false, title.." "..msg
			end
			modSoundChat.doSoundPlayer(sendername, "sfx_chat2", 0)
			modSoundChat.showFormspecHistoric(sendername, playername)
			return true
		end,
	}
end


modSoundChat.doChatMessage = function(sendername, msg)
	if minetest.get_player_privs(sendername).shout then
		local sender = minetest.get_player_by_name(sendername)
		
		local mode = modSoundChat.getChatMode(sendername)
		local pvtname = modSoundChat.getPvtName(sendername)
		local sblMode = ""
		local sblModeStriped = ""
		if mode == "local" then
			sblModeStriped = "[local]"
			sblMode = core.colorize("#888888", "["..modSoundChat.translate("local").."]")
		elseif mode == "global" then
			sblModeStriped = "[global]"
			sblMode = core.colorize("#888888", "["..modSoundChat.translate("global").."]")
		elseif mode == "private" then
			sblModeStriped = "["..("private to '%s'"):format(pvtname).."]"
			sblMode = core.colorize("#888888", "["..modSoundChat.translate(("private to '%s'"):format(pvtname)).."]")
		end
		
		local msgform = ""
		local msgformstriped = ""
		if minetest.get_player_privs(sendername).server then
			msgform = sblMode.." "..core.colorize(modSoundChat.getColorNameAdmin(), sendername.." (Admin): ")..msg
			msgformstriped = sblModeStriped.." "..sendername.." (Admin): "..msg
		elseif minetest.global_exists("modEUrn") 
			and type(modEUrn.getPresidentName())=="string"  --<string/nil> modEUrn.getPresidentName()
			and modEUrn.getPresidentName() == sendername 
		then 
			msgform = sblMode.." "..core.colorize("#FF00FF", sendername.." ("..modSoundChat.translate("President").."): ")..msg
			msgformstriped = sblModeStriped.." "..sendername.." (President): "..msg
		else
			local sent = false
			if modSoundChat.count_nametags >= 1 then
				for tag_name,tag_def in pairs(modSoundChat.ChatTags) do
					if 
						tag_def.priv ~= nil 
						and type(tag_def.priv)=="string" 
						and tag_def.priv ~= "" 
						and minetest.get_player_privs(sendername)[tag_def.priv] 
					then
						sent = true
						msgform = sblMode.." "..core.colorize(tag_def.color, sendername.." ("..modSoundChat.translate(tag_name).."): ")..msg
						msgformstriped = sblModeStriped.." "..sendername.." ("..tag_name.."): "..msg
						break
					end
				end
			end
			if sent == false then
				msgform = sblMode.." "..core.colorize(modSoundChat.getColorNamePlayer(), sendername..": ")..msg
				msgformstriped = sblModeStriped.." "..sendername..": "..msg
			end
		end
		
		if modSoundChat.isPrintTerminalDialogs() then
    		print(msgformstriped)
		end
		
		local listeners = 0 --Counts how many people heard the message sent.
		local maxdist = modSoundChat.getMaxDialogDist()
		for i,player in ipairs(minetest.get_connected_players()) do
			if player and player:is_player() and player:get_player_name() then
				local playername = player:get_player_name()
				if not modSoundChat.isIgnored(sendername, playername) then --Saber se ignora o receptor da mensagem.
					local isSuperEar = minetest.get_player_privs(playername).superear
					local dist = modSoundChat.getDistance(sender, player)
					if isSuperEar
						or (
							mode == "local" 
							and maxdist >= 1 
							and dist ~= nil 
							and type(dist) == "number" 
							and dist <= maxdist
						)
						or (
							mode == "global" 
							or maxdist <= 0
						) 
						or mode == "private" and (
							sendername == playername --A pessoa recebe a propria fala que ele mesmo digitou.
							or ( --
								pvtname ~= nil 
								and type(pvtname) == "string" 
								and pvtname  == playername
							)
						)
					then
						minetest.chat_send_player(playername, msgform)
						modSoundChat.addHistoric(playername, msgformstriped)
						if modSoundChat.isSaveDialogs() then
   						--[[  
   						modSoundChat.debug("modSoundChat.doChatMessage() >>> modSoundChat.isSaveDialogs()=".. dump(modSoundChat.isSaveDialogs()), sendername)
   						modSoundChat.debug("modSoundChat.doChatMessage() >>> minetest.global_exists('libSaveLogs')=".. dump(minetest.global_exists("libSaveLogs")), sendername)
   						modSoundChat.debug("modSoundChat.doChatMessage() >>> sender:is_player()=".. dump(sender:is_player()), sendername)
   						modSoundChat.debug("modSoundChat.doChatMessage() >>> player:is_player()=".. dump(player:is_player()), sendername)
   						modSoundChat.debug("modSoundChat.doChatMessage() >>> (sendername == playername)=".. dump(sendername == playername), sendername)
   						--[[  ]]
   						if minetest.global_exists("libSaveLogs")
   						   and sender and sender:is_player() 
   						   and player and player:is_player() 
   						   and sendername == playername
   						then
   						   local text_pos = ""
   						   if type(libSaveLogs.savePosOfSpeaker)=="boolean" 
   						      and libSaveLogs.savePosOfSpeaker==true 
   						   then
      						   local posSender = sender:get_pos()
   						      text_pos = " "..minetest.pos_to_string(libSaveLogs.getPosResumed(posSender))
   						   end
   						   --modSoundChat.debug("modSoundChat.doChatMessage() >>> "..minetest.strip_colors(msgform)..text_pos, sendername)
      						modSoundChat.doSaveLog(
      						   --"[SOUNDCHAT] "..
      						   --"###################"..
      						   modSoundChat.stripTagTranslate(msgformstriped)
      						   ..text_pos
      						   , false
      						)
   						end
						end --Final of: if modSoundChat.isSaveDialogs() then
						
						
						listeners = listeners + 1
						
						if modSoundChat.isEnabled() and type(msg)=="string" and msg:len()>=3 then
							if playername~=sendername then --Toca para todos ,exceto para quem enviou a mensagem.
								if not modSoundChat.players[playername] then 
									modSoundChat.players[playername] = { }
								end
								if modSoundChat.players[playername].handler ~=nil then 
									minetest.sound_stop(modSoundChat.players[playername].handler)
								end
								modSoundChat.players[playername].mute = (type(modSoundChat.players[playername].mute)=="boolean" and modSoundChat.players[playername].mute==true)
								modSoundChat.players[playername].muteCall = (type(modSoundChat.players[playername].muteCall)=="boolean" and modSoundChat.players[playername].muteCall==true)
							
								if tostring(msg):lower():find(tostring(playername):lower())	or (tostring(msg):len()>=4 and tostring(playername):len()>=4 and tostring(msg):lower():find(tostring(playername):lower())) then 
									--#################### CHAMAR ATENÇÃO #########################################################
									if not modSoundChat.players[playername].muteCall then
									   if modSoundChat.isCall.PlayerName() then
											modSoundChat.players[playername].handler = modSoundChat.doSoundPlayer(playername, "sfx_chat_playername", 0)
										end
										minetest.chat_send_player(playername, 
											--core.get_color_escape_sequence("#ffff00")..sendername..core.get_color_escape_sequence("#00ffff").." citou seu nome!"
											core.colorize("#FF00FF", "[SOUNDCHAT] ")
											..modSoundChat.translate("The player '%s' mentioned your name!"):format(core.colorize("#FFFF00", sendername))
										)
									end
								elseif not modSoundChat.players[playername].mute then 
									--#################### CONVERSA COMUM #########################################################
									if modSoundChat.isCall.SendMessage() then
										modSoundChat.players[playername].handler = modSoundChat.doSoundPlayer(playername, "sfx_chat_speak", 0)
									end
								end
							end
						end --Fim de if modSoundChat.isEnabled() and msg and msg:len()>=2 then
						
					end --FIM if maxdist == 0 or dist <= maxdist then
				end --FIM if modSoundChat.isIgnored(sendername, playername) then
			end --FIM if player and player:is_player() and player:get_player_name() then
		end --Fim de for
		if 
			listeners < 2 -- Me self and other people 
			and sendername ~= "singleplayer"
			and modSoundChat.getChatMode(sendername) ~= "global"
		then --At least the player sending the message must hear what he himself wrote.
			minetest.chat_send_player(sendername, 
				--core.get_color_escape_sequence("#ffff00")..sendername..core.get_color_escape_sequence("#00ffff").." citou seu nome!"
				core.colorize("#FFFF00", "[SOUNDCHAT] ")
				..core.colorize("#888888", modSoundChat.translate("No player is close or interested enough to hear what you said!"))
			)
			modSoundChat.doSoundPlayer(sendername, "sfx_error", 0)
		end
		return true
	end --if minetest.get_player_privs(sendername).shout then
end

--#########################################################################################################################################

modSoundChat.propCommandDoSoundChat = function()
	return { 
		params="", 
		privs={},
		description = modSoundChat.translate("Show settings of Sound Chat."),
		func = function(playername, param)
			modSoundChat.doSoundPlayer(playername, "sfx_chat2", 0)
			modSoundChat.showMainFormspec(playername)
			--return true
		end,
	}
end

modSoundChat.propCommandDoLocalChat = function()
	return { 
		params="", 
		privs={},
		description = modSoundChat.translate("Enable chat in LOCAL mode."),
		func = function(playername, param)
			modSoundChat.setChatMode(playername, "local", nil)
			modSoundChat.doSoundPlayer(playername, "sfx_chat2", 0)
			return true
		end,
	}
end

modSoundChat.propCommandDoGlobalChat = function()
	return { 
		params="", 
		privs={},
		description = modSoundChat.translate("Enable chat in GLOBAL mode. (Need the priv 'globalchat')"),
		func = function(playername, param)
			if modSoundChat.setChatMode(playername, "global", nil) then
				modSoundChat.doSoundPlayer(playername, "sfx_chat2", 0)
			else
				modSoundChat.doSoundPlayer(playername, "sfx_error", 0)
			end
			return true
		end,
	}
end

modSoundChat.propCommandDoPrivateChat = function()
	return { 
		params="<playername>", 
		privs={},
		description = modSoundChat.translate("Enable chat in PRIVATE mode."),
		func = function(playername, param)
			--modSoundChat.setChatMode(playername_from, mode, playername_to)
			if type(param)=="string"
			   and param ~= ""
			   and modSoundChat.setChatMode(playername, "private", param) then
				modSoundChat.doSoundPlayer(playername, "sfx_chat2", 0)
			else
				--modSoundChat.doSoundPlayer(playername, "sfx_error", 0)
				modSoundChat.showPrivatesFormspec(playername)
			end
			return true
		end,
	}
end

modSoundChat.propCommandDoIgnored = function()
	return { 
		params="<playername>", 
		privs={},
		description = modSoundChat.translate("Add or Remove a player from your chat ignore list. (Do not add admin player)"),
		func = function(playername, param)
			if modSoundChat.doChangeIgnored(playername, param) then
				modSoundChat.doSoundPlayer(playername, "sfx_chat2", 0)
			else
				modSoundChat.doSoundPlayer(playername, "sfx_error", 0)
			end
			return true
		end,
	}
end

modSoundChat.propCommandMute = function()
	return { 
		params="", 
		privs={},
		description = modSoundChat.translate("Enables and disables the simple sound of your own individual chat. (Do not disable admin alarm)"),
		func = function(playername, param)
			modSoundChat.doMute(playername)
			modSoundChat.doSoundPlayer(playername, "sfx_chat2", 0)
			return true
		end,
	}
end

modSoundChat.propCommandMuteCall = function()
	return { 
		params="", 
		privs={},
		description = modSoundChat.translate("Enables and disables the alarm sound of call your name."),
		func = function(playername, param)
			modSoundChat.doMuteCall(playername)
			modSoundChat.doSoundPlayer(playername, "sfx_chat2", 0)
			return true
		end,
	}
end

modSoundChat.propCommandAlert = function()
	return {
		params = "<message>",
		description = modSoundChat.translate("Send a colored warning with flashy sound for all players (Need the priv 'server')"),
		privs = {server=true},
		func = function(playername, params)
			return modSoundChat.doAlert(playername, params)
		end,
	}
end

modSoundChat.propCommandShutdownAlert = function()
   return {
      params = "<secound>",
      description = modSoundChat.translate("Shutdown the server after a public message and a alert sound."),
      privs = {server = true},
      func = function(sendername, param)
         modSoundChat.doShutdownAlert(sendername, param)
      end,
   }
end 


