-- #### UNREGISTER COMMANDS ########################

minetest.unregister_chatcommand("whisper") --Comando do mod 'lib_savechat'
minetest.unregister_chatcommand("msg") --Comando do mod 'lib_savechat'
minetest.unregister_chatcommand("pvt") --Comando do mod 'lib_savechat'
minetest.unregister_chatcommand("me") --Comando do mod 'lib_savechat'

--##################################################

minetest.register_chatcommand("soundchat", modSoundChat.propCommandDoSoundChat())
minetest.register_chatcommand("schat", modSoundChat.propCommandDoSoundChat())
minetest.register_chatcommand("sc", modSoundChat.propCommandDoSoundChat())

--##################################################

minetest.register_chatcommand("localchat", modSoundChat.propCommandDoLocalChat())
minetest.register_chatcommand("lchat", modSoundChat.propCommandDoLocalChat())
minetest.register_chatcommand("lc", modSoundChat.propCommandDoLocalChat())

--##################################################

minetest.register_chatcommand("globalchat", modSoundChat.propCommandDoGlobalChat())
minetest.register_chatcommand("gchat", modSoundChat.propCommandDoGlobalChat())
minetest.register_chatcommand("gc", modSoundChat.propCommandDoGlobalChat())

--##################################################

minetest.register_chatcommand("privatechat", modSoundChat.propCommandDoPrivateChat())
minetest.register_chatcommand("pvtchat", modSoundChat.propCommandDoPrivateChat())
minetest.register_chatcommand("pchat", modSoundChat.propCommandDoPrivateChat())
minetest.register_chatcommand("pc", modSoundChat.propCommandDoPrivateChat())

--##################################################

minetest.register_chatcommand("historicchat", modSoundChat.propCommandShowHistoric())
minetest.register_chatcommand("histchat", modSoundChat.propCommandShowHistoric())
minetest.register_chatcommand("hc", modSoundChat.propCommandShowHistoric())

--##################################################

minetest.register_chatcommand("ignore", modSoundChat.propCommandDoIgnored())
minetest.register_chatcommand("ign", modSoundChat.propCommandDoIgnored())

--##################################################

minetest.register_chatcommand("mute", modSoundChat.propCommandMute())
--minetest.register_chatcommand("mudo", modSoundChat.propCommandMute())

--##################################################

minetest.register_chatcommand("mutecall", modSoundChat.propCommandMuteCall())
--minetest.register_chatcommand("nomemudo", modSoundChat.propCommandMuteCall())
--minetest.register_chatcommand("emudecerchamada", modSoundChat.propCommandMuteCall())

--##################################################

minetest.register_chatcommand("alert", modSoundChat.propCommandAlert())

--##################################################

minetest.register_chatcommand('smartshutdown', modSoundChat.propCommandShutdownAlert())
minetest.register_chatcommand('sshutdown', modSoundChat.propCommandShutdownAlert())
minetest.register_chatcommand('ssd', modSoundChat.propCommandShutdownAlert())


