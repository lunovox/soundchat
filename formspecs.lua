modSoundChat.showMainFormspec = function(playername)
   local mode = modSoundChat.getChatMode(playername)
	local myModeLocal = modSoundChat.translate("LOCAL")
   local myModeGlobal = modSoundChat.translate("GLOBAL")
   local myModePrivate = modSoundChat.translate("PRIVATE").." ..."
   local myModeAFK = modSoundChat.translate("AFK").." ..."
   if mode == "local" then 
   	myModeLocal = core.colorize("#00FFFF", myModeLocal) 
   elseif mode == "global" then 
   	myModeGlobal = core.colorize("#00FFFF", myModeGlobal) 
   elseif mode == "private" then 
   	myModePrivate = core.colorize("#00FFFF", myModePrivate) 
   elseif mode == "afk" then 
   	myModeAFK = core.colorize("#00FFFF", myModeAFK) 
   end
   
   myModeAFK = core.colorize("#888888", myModeAFK) 
   
   modSoundChat.players[playername].mute = (type(modSoundChat.players[playername].mute)=="boolean" and modSoundChat.players[playername].mute==true)
	modSoundChat.players[playername].muteCall = (type(modSoundChat.players[playername].muteCall)=="boolean" and modSoundChat.players[playername].muteCall==true)

   local myCommonBell = core.colorize("#00FF00", "ON")
   if modSoundChat.players[playername].mute then
   	myCommonBell = core.colorize("#FF0000", "OFF")
   end
   local myNameAlarms = core.colorize("#00FF00", "ON")
   if modSoundChat.players[playername].muteCall then
   	myNameAlarms = core.colorize("#FF0000", "OFF")
   end
   
   local formspec = "size[7.5,5.5]" --Help: https://minetest.gitlab.io/minetest/formspec/
	.."formspec_version[7]"
	--bgcolor[color;fullscreen;]
	.."bgcolor[#636D76FF;false]"
	--..default.gui_bg
	--..default.gui_bg_img
	--..default.gui_slots
	--..default.get_hotbar_bg(x,y) --Source: https://github.com/appgurueu/minetest_game/blob/master/mods/default/init.lua
	--.. default.gui_bg
	--.. default.gui_bg_img
	--.."bgcolor[#080808BB;true]"
	.."background9[5,5;1,1;gui_formbg.png;true;10]" --Source: https://github.com/appgurueu/minetest_game/blob/master/mods/default/init.lua
	.."background[5,5;1,1;gui_formbg.png;true]" --Textures: https://github.com/appgurueu/minetest_game/tree/master/mods/default/textures
	--.."image[4.75,1.5;1,1;gui_furnace_arrow_bg.png^[transformR270]"

	--[[ --Source: https://minetest.gitlab.io/minetest/formspec/
	..[[
		formspec_version[5]
		hypertext[]
		<global background=#FFFFFF margin=15 valign=middle color=#000000 hovercolor=#0000FF size=3 font=normal halign=justify >
		<normal>testo de tamanho Normal</normal>
		<big>testo maior</big>
		<bigger>testo gigante</bigger>
		<center>centro</center>
		<left>esquerda</left>
		<right>direita</right>
		<justify>Justificado</justify>
		<mono>mono espaçado</mono>
		<b>Bold</b>, <i>italic</i>, <u>underline styles</u>
		<action name=lnkMeuLink> 
			texto clicável. O campo vai para 'on_player_receive_fields' como 'action:lnkMeuLink' 
		</action>
		<img name=favicon.png float=right width=30 height=30>
	]]
	--]]
	
	.."box[0,0;3,4.5;#000000CC]"
	.."label[0.1,0.1;"..minetest.formspec_escape(modSoundChat.translate("CHAT MODE")..":").."]"	
	.."button[0.25,0.75;2.65,0.5;btnChatLocal;"..minetest.formspec_escape(myModeLocal).."]"
	.."button[0.25,1.75;2.65,0.5;btnChatGlobal;"..minetest.formspec_escape(myModeGlobal).."]"
	.."button[0.25,2.75;2.65,0.5;btnChatPrivates;"..minetest.formspec_escape(myModePrivate).."]"
	.."button[0.25,3.75;2.65,0.5;btnChatAFK;"..minetest.formspec_escape(myModeAFK).."]"

	.."box[3.25,0;4,2.5;#000000CC]"
	.."label[3.35,0.1;"..minetest.formspec_escape(modSoundChat.translate("SOUND MODE")..":").."]"
	--checkbox[<X>,<Y>;<name>;<label>;<selected>]
	.."button[3.50,0.75;3.65,0.5;btnMuteCommonBell;"..minetest.formspec_escape(modSoundChat.translate("Common Bell")..": "..myCommonBell).."]"
	.."button[3.50,1.75;3.65,0.5;btnMuteNameAlarms;"..minetest.formspec_escape(modSoundChat.translate("Name Alarms")..": "..myNameAlarms).."]"
	
	--tooltip[<gui_element_name>;<tooltip_text>;<bgcolor>;<fontcolor>]
	.."tooltip[button;"..minetest.formspec_escape(modSoundChat.translate("Show settings of ignored players"))..";#88880088;#000000]" 
	.."button[3.50,2.85;3.65,0.5;btnChatIgnoreds;"..minetest.formspec_escape(modSoundChat.translate("IGNOREDS").." ...").."]"
	
	.."tooltip[button;"..minetest.formspec_escape(
	   modSoundChat.translate("Show the historic of chat.")..'\n'
	   ..modSoundChat.translate("Ideal for smartphone gamers.")
	)..";#88880088;#000000]" 
	.."button[3.50,3.85;3.65,0.5;btnChatHistoric;"..minetest.formspec_escape(modSoundChat.translate("CHAT HIST").." ...").."]"
	
	.."box[0,4.75;7.25,0.90;#00000044]"
	.."tooltip[button;;#88880088;#000000]"
	.."button_exit[2.50,5;2,0.5;;"..minetest.formspec_escape(modSoundChat.translate("CLOSE")).."]"

	minetest.show_formspec(playername,"soundchat.showMainFormSpec",formspec)
end

modSoundChat.showPrivatesFormspec = function(playername)
	local tblOnlines = minetest.get_connected_players()
	local txtPrivates = ""
	local selPrivate = "" --<== É bom por "" ao invez de 0 pq o campo autoseleciona
	local playerCount = 0 --<== tem que contar pq tem todo player entra na lista
	for _, player in ipairs(tblOnlines) do
		local playerNameOnline = ""
		if playername ~= player:get_player_name() then
			playerCount = playerCount + 1
			if txtPrivates == "" then
				txtPrivates = minetest.formspec_escape(player:get_player_name())
			else
				txtPrivates = txtPrivates..","..minetest.formspec_escape(player:get_player_name())
			end
			if modSoundChat.getPvtName(playername) == player:get_player_name() then
				--selPrivate = minetest.formspec_escape(player:get_player_name())
				selPrivate = playerCount
			end
		end
	end

	--local tblPrivates = modSoundChat.getListPrivates(playername)
	--local txtPrivates = table.concat(Privates, ",")
	local formspec = "size[5.5,3.15]"

	.."bgcolor[#636D76FF;false]" 
	--bgcolor[color;fullscreen;]
	--..default.gui_bg
	--..default.gui_bg_img
	--..default.gui_slots
	--..default.get_hotbar_bg(x,y) --Source: https://github.com/appgurueu/minetest_game/blob/master/mods/default/init.lua
	--.. default.gui_bg
	--.. default.gui_bg_img
	--.."bgcolor[#080808BB;true]"
	--.."background9[5,5;1,1;gui_formbg.png;true;10]" --Source: https://github.com/appgurueu/minetest_game/blob/master/mods/default/init.lua
	--.."background[5,5;1,1;gui_formbg.png;true]" --Source: https://github.com/appgurueu/minetest_game/blob/master/mods/default/init.lua
	--Textures: https://github.com/appgurueu/minetest_game/tree/master/mods/default/textures
	
	.."box[0,0;5.25,2.5;#000000CC]"
	.."label[0.25,0.1;"..minetest.formspec_escape(modSoundChat.translate("PLAYERS ONLINE")..":").."]"
	--textlist[<X>,<Y>;<W>,<H>;<name>;<listelem 1>,<listelem 2>,...,<listelem n>;<selected idx>;<transparent>]
	.."dropdown[0.25,0.75;5.25,0.25;selPrivate;"..txtPrivates..";"..selPrivate.."]"
	--.."textlist[0.25,0.75;4.75,2;selPrivate;"..txtPrivates..";"..selPrivate..";false]"
	.."button_exit[0.25,1.55;5,1;btnSelPrivate;"..minetest.formspec_escape(modSoundChat.translate("START PRIVATE CHAT")).."]"
	.."set_focus[btnSelPrivate;true]"  --set_focus[<name>;<force>]
	--.."box[0,4.25;5.25,2.5;#000000CC]"
	--.."field[0.5,5.1;5,1;txtNewIgnored;"..minetest.formspec_escape(modSoundChat.translate("Player Name"))..";]"
	--.."pwdfield[0.29,4.25;4,1;txtNewGuest;Nome do Convidado]" --Line for Password
	--.."button[0.25,5.75;5,1;btnNewIgnored;"..minetest.formspec_escape(modSoundChat.translate("Add to Ignored List")).."]"
	
	--.."button[0,2.75;2.5,0.5;btnShowMainForm;"..minetest.formspec_escape(modSoundChat.translate("BACK")).."]"
	.."button_exit[2.95,2.75;2.5,0.5;;"..minetest.formspec_escape(modSoundChat.translate("CLOSE")).."]"
	
	minetest.show_formspec(playername,"soundchat.showPrivatesFormSpec",formspec)

end

modSoundChat.showIgnoredsFormspec = function(playername)
	local tblIgnoreds = modSoundChat.getListIgnoreds(playername)
	local txtIgnoreds = table.concat(tblIgnoreds, ",")
	--[[
	minetest.chat_send_all(
		"tblIgnoreds = "..dump(tblIgnoreds)
		.." | txtIgnoreds = "..dump(txtIgnoreds)
	)
	--]]
	local formspec = "size[5.5,7.5]"

	.."bgcolor[#636D76FF;false]" --bgcolor[color;fullscreen;]
	--..default.gui_bg
	--..default.gui_bg_img
	--..default.gui_slots
	--..default.get_hotbar_bg(x,y) --Source: https://github.com/appgurueu/minetest_game/blob/master/mods/default/init.lua
	--.. default.gui_bg
	--.. default.gui_bg_img
	--.."bgcolor[#080808BB;true]"
	--.."background9[5,5;1,1;gui_formbg.png;true;10]" --Source: https://github.com/appgurueu/minetest_game/blob/master/mods/default/init.lua
	--.."background[5,5;1,1;gui_formbg.png;true]" --Source: https://github.com/appgurueu/minetest_game/blob/master/mods/default/init.lua
	--Textures: https://github.com/appgurueu/minetest_game/tree/master/mods/default/textures
	
	.."box[0,0;5.25,4.0;#000000CC]"
	.."label[0.25,0.1;"..minetest.formspec_escape(modSoundChat.translate("IGNORED PLAYERS")..":").."]"
	--textlist[<X>,<Y>;<W>,<H>;<name>;<listelem 1>,<listelem 2>,...,<listelem n>;<selected idx>;<transparent>]
	.."textlist[0.25,0.75;4.75,2;selIgnoreds;"..txtIgnoreds..";0;false]"
	.."button[0.25,3.0;5,1;btnDelIgnored;"..minetest.formspec_escape(modSoundChat.translate("Remove Player")).."]"
	
	.."box[0,4.25;5.25,2.5;#000000CC]"
	.."field[0.5,5.1;5,1;txtNewIgnored;"..minetest.formspec_escape(modSoundChat.translate("Player Name"))..";]"
	--.."pwdfield[0.29,4.25;4,1;txtNewGuest;Nome do Convidado]" --Line for Password
	.."button[0.25,5.75;5,1;btnNewIgnored;"..minetest.formspec_escape(modSoundChat.translate("Add to Ignored List")).."]"
	
	.."button[0,7.1;2.5,0.5;btnShowMainForm;"..minetest.formspec_escape(modSoundChat.translate("BACK")).."]"
	.."button_exit[2.75,7.1;2.5,0.5;;"..minetest.formspec_escape(modSoundChat.translate("CLOSE")).."]"
	
	minetest.show_formspec(playername,"soundchat.showIgnoredFormSpec",formspec)
end

modSoundChat.showFormspecHistoric = function(sendername, playername)
	local historic = modSoundChat.getHistoric(playername)
	local formspec = ""
	formspec = formspec
		.."formspec_version[6]"
		.."size[20,8,false]"
		--.."background[0,-8;16,16;text_eurn_front.png]"
		.."box[0.5,0.75;19,5.75;#000000CC]"
		.."textarea[0.5,0.75;19,5.75;;"
			..minetest.formspec_escape(
				modSoundChat.translate("Dialogue History of '%s'"):format(dump(playername))..":"
			)..";"
			..minetest.formspec_escape(historic)..
		"]"
		.."button_exit[16.85,6.75;2.55,1;;"..modSoundChat.translate("CLOSE").."]"

	minetest.show_formspec(sendername,"soundchat.formHistoric",formspec)
end


modSoundChat.doReceiveFields = function(sender, formname, fields)
	local sendername = sender:get_player_name()
	--modSoundChat.debug("modSoundChat.doReceiveFields() : "..sendername.." >>> ["..formname.."] = "..dump2(fields))
	--print(sendername.." >>> ["..formname.."] = "..dump(fields))
	if formname == "soundchat.showMainFormSpec"  then
		--[[ 
		minetest.chat_send_all(
			"minetest.register_on_player_receive_fields("..sendername..", "..formname..") >>> "..dump(fields)
		)
		--]]
		if fields.btnChatLocal ~= nil then
			local mode = modSoundChat.getChatMode(sendername)
			if mode ~= "local" then
				modSoundChat.setChatMode(sendername, "local", nil)
				modSoundChat.showMainFormspec(sendername)
			end
		elseif fields.btnChatGlobal ~= nil then
			local mode = modSoundChat.getChatMode(sendername)
			if mode ~= "global" then
				if modSoundChat.setChatMode(sendername, "global", nil) ~= "global" then
					modSoundChat.doSoundPlayer(sendername, "sfx_error", 0)
				end
				modSoundChat.showMainFormspec(sendername)
			end
		elseif fields.btnChatPrivates ~= nil then
			modSoundChat.showPrivatesFormspec(sendername)
		elseif fields.btnChatAFK ~= nil then
			--modSoundChat.showPrivatesFormspec(sendername)
			modSoundChat.doSoundPlayer(sendername, "sfx_error", 0)
		elseif fields.btnMuteCommonBell ~= nil then
			modSoundChat.doMute(sendername)
			modSoundChat.showMainFormspec(sendername)
		elseif fields.btnMuteNameAlarms ~= nil then
			modSoundChat.doMuteCall(sendername)
			modSoundChat.showMainFormspec(sendername)
		elseif fields.btnChatIgnoreds ~= nil then
			modSoundChat.showIgnoredsFormspec(sendername)
		elseif fields.btnChatHistoric ~= nil then
			modSoundChat.showFormspecHistoric(sendername, sendername)
		end
		--[[ 
		minetest.chat_send_all(
			"minetest.register_on_player_receive_fields("..sendername..", "..formname..") >>> "..dump(fields)
		)
		--]]
	elseif formname == "soundchat.showPrivatesFormSpec"  then
		--[[ 
		minetest.chat_send_all(
			"minetest.register_on_player_receive_fields("..sendername..", "..formname..") >>> "..dump(fields)
		)
		--]]
		if fields.btnSelPrivate ~= nil then
			if type(fields.selPrivate)=="string" and fields.selPrivate~="" then
				--[[  ]]
				--minetest.get_player_by_name(fields.selPrivate):is_player()
				if modSoundChat.setChatMode(sendername, "private", fields.selPrivate) then
					modSoundChat.doSoundPlayer(sendername, "sfx_chat2", 0)
					--modSoundChat.showMainFormspec(sendername)
				else
					modSoundChat.doSoundPlayer(sendername, "sfx_error", 0)
				end
				--]]
			else
				minetest.chat_send_player(sendername, 
					--core.get_color_escape_sequence("#ffff00")..sendername..core.get_color_escape_sequence("#00ffff").." citou seu nome!"
					core.colorize("#FFFF00", "[SOUNDCHAT:ALERT] ")
					..core.colorize("#888888", 
						modSoundChat.translate("Select a player to activate chat in private mode!")
					)
				)
				modSoundChat.doSoundPlayer(sendername, "sfx_error")
			end
		elseif fields.btnShowMainForm ~= nil then
			modSoundChat.showMainFormspec(sendername)
		end
	elseif formname == "soundchat.showIgnoredFormSpec"  then
		--[[ 
		minetest.chat_send_all(
			"minetest.register_on_player_receive_fields("..sendername..", "..formname..") >>> "..dump(fields)
		)
		--]]
		
		if type(modSoundChat.selIgnored)~="table" then
			modSoundChat.selIgnored = {}
		end
		if fields.selIgnoreds ~= nil then
		   local selIgnored = minetest.explode_textlist_event(fields.selIgnoreds)
   		if type(selIgnored.index)=="number" and selIgnored.index>=1 then
   		   local tblIgnoreds = modSoundChat.getListIgnoreds(sendername)
   		   modSoundChat.selIgnored[sendername] = tblIgnoreds[selIgnored.index]
   		   --modSoundChat.debug("modSoundChat.doReceiveFields() : modSoundChat.selIgnored[sendername] = "..modSoundChat.selIgnored[sendername], sendername)
   		end
		elseif fields.btnDelIgnored ~= nil then
			--Botão Apagar Ignorado da Lista
			--modSoundChat.debug("type(modSoundChat.selIgnored[sendername]) = "..type(modSoundChat.selIgnored[sendername]), sendername)
			if type(modSoundChat.selIgnored[sendername])=="string" and modSoundChat.selIgnored[sendername] ~= "" then
			   local selIgnored = modSoundChat.selIgnored[sendername]
			   modSoundChat.debug("modSoundChat.isIgnored("..sendername..", "..selIgnored..") = "..dump(modSoundChat.isIgnored(sendername, selIgnored)))
			   if modSoundChat.isIgnored(sendername, selIgnored) then
			      local result = modSoundChat.doChangeIgnored(sendername, selIgnored)
				   if result == true then
		            modSoundChat.showIgnoredsFormspec(sendername)
			      end
			   end
	      end
		elseif fields.btnNewIgnored ~= nil and fields.txtNewIgnored ~= nil and fields.txtNewIgnored ~= "" then
			--Botão Novo Ignorado
			if not modSoundChat.isIgnored(sendername, fields.txtNewIgnored) then
				--modSoundChat.players[sendername].ignoreds[fields.txtNewIgnored] = true
				local result = modSoundChat.doChangeIgnored(sendername, fields.txtNewIgnored)
				if result == true then
					modSoundChat.showIgnoredsFormspec(sendername)
				end
			else
				local msg = modSoundChat.translate(
					"The player '%s' was already on your ignore list!"
				):format(fields.txtNewIgnored)
				modSoundChat.doSaveLog(msg, true)
				minetest.chat_send_player(sendername, 
					core.colorize("#FF0000", "[SOUNDCHAT:ERRO]").." "
					..msg 
				)
				modSoundChat.doSoundPlayer(sendername, "sfx_error", 0)
				modSoundChat.showIgnoredsFormspec(sendername)
			end
		elseif fields.btnShowMainForm ~= nil then
			modSoundChat.showMainFormspec(sendername)
		end
	end
	-- -- -- --return fields --Não retorne o field pq isso atrapalha outros mods
end
