modSoundChat = { 
	players = { },
	modname = minetest.get_current_modname(),
	modpath = minetest.get_modpath(minetest.get_current_modname()),
	database_file = minetest.get_worldpath().."/soundchat.db"
}

dofile(modSoundChat.modpath.."/translate.lua") -- <== Antes de 'api.lua'!
dofile(modSoundChat.modpath.."/nametags.lua")
dofile(modSoundChat.modpath.."/api.lua")
dofile(modSoundChat.modpath.."/formspecs.lua")
dofile(modSoundChat.modpath.."/commands.lua")
dofile(modSoundChat.modpath.."/menu_plus.lua")
dofile(modSoundChat.modpath.."/item_computing_app.lua")


--#########################################################################################################################################


minetest.register_privilege("globalchat",  {
	description=modSoundChat.translate("Allows you to enable chat in GLOBAL mode by command, to send message to all players, regardless of distance."), 
	give_to_singleplayer=false,
})

minetest.register_privilege("superear",  {
	description=modSoundChat.translate("Allows you to read all messages sent by the player in real time, no matter how far away."), 
	give_to_singleplayer=false,
})

--minetest.register_on_receiving_chat_message(function(sendername,msg)
--minetest.register_on_sending_chat_message(function(sendername,msg)
--minetest.register_on_chat_message(modSoundChat.doChatMessage)
--[[ ]]
minetest.register_on_chat_message(function(sendername, msg)
	return modSoundChat.doChatMessage(sendername, msg)
end)
--]]

minetest.register_on_player_receive_fields(function(sender, formname, fields)
	modSoundChat.doReceiveFields(sender, formname, fields)
end)

minetest.register_on_joinplayer(function(player)
	local playername = player:get_player_name()
	if not modSoundChat.players[playername] then 
		modSoundChat.players[playername] = { }
	end
	modSoundChat.doSoundPlayer(playername, "sfx_login", 32000)
	--modSoundChat.setChatMode(playername_from, mode, playername_to)
	--modSoundChat.setChatMode(playername, "local", nil)
	modSoundChat.players[playername].chatmode = "local"
	modSoundChat.doNormalizeNametag(playername)
end)

minetest.register_on_leaveplayer(function(player)
	modSoundChat.dbSave()
	local playername = player:get_player_name()
	modSoundChat.doSoundPlayer(playername, "sfx_logout", 32000)
end)

minetest.register_on_shutdown(function()
   modSoundChat.dbSave()
end)

modSoundChat.dbLoad()

minetest.log('action',"["..modSoundChat.modname.."] "..modSoundChat.translate("Mod Loaded!"))
