

if core.global_exists("modComputing") then
   modComputing.add_app("soundchat:btnSoundChat", {
   	icon_name = "btnSoundChat",
   	icon_title = modSoundChat.translate("SOUNDCHAT"),
   	icon_descryption = modSoundChat.translate(
   	   "Access the chat sound settings."
   	),
   	icon_type = "button", --types: button/button_exit
   	icon_image = "icon_soundchat.png",
   	on_iconclick = function(player)
   	   local playername = player:get_player_name()
   	   core.log(
			   "action","[SOUNDCHAT] "
			   ..modCorreio.translate(
               "Player '@1' is trying to access the '@2' via the '@3'!"
               , playername
               , "Chat Sound Settings"
               , "computing app"
            )
         )
			modSoundChat.showMainFormspec(playername)
   	end,
   })

   modComputing.add_app("soundchat:btnPrivateChat", {
   	icon_name = "btnPrivateChat",
   	icon_title = modSoundChat.translate("PRIV.CHAT"),
   	icon_descryption = modSoundChat.translate(
   	   "Start a private chat with a specific player over infinite distances."
   	),
   	icon_type = "button", --types: button/button_exit
   	icon_image = "icon_privatechat_mask.png",
   	on_iconclick = function(player)
   	   local playername = player:get_player_name()
   	   core.log(
			   "action","[SOUNDCHAT] "
			   ..modCorreio.translate(
               "Player '@1' is trying to access the '@2' via the '@3'!"
               , playername
               , "Private Chat Form"
               , "computing app"
            )
         )
			modSoundChat.showPrivatesFormspec(playername)
   	end,
   })
   
   modComputing.add_app("soundchat:btnChatHistoric", {
   	icon_name = "btnChatHistoric",
   	icon_title = modSoundChat.translate("HISTORIC"),
   	icon_descryption = modSoundChat.translate(
   	   "Show your chat historic."
   	),
   	icon_type = "button", --types: button/button_exit
   	icon_image = "icon_chat_historic.png",
   	on_iconclick = function(player)
   	   local playername = player:get_player_name()
   	   core.log(
			   "action","[SOUNDCHAT] "
			   ..modCorreio.translate(
               "Player '@1' is trying to access the '@2' via the '@3'!"
               , playername
               , "Chat Historic"
               , "computing app"
            )
         )
			modSoundChat.showFormspecHistoric(playername, playername)
   	end,
   })
end