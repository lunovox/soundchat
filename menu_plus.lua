--[[
	Esse código foi inspitado do mod Edit_skin

--]]
--------------------------------------------------------------------------------------------------------------

if minetest.global_exists("sfinv_buttons") then
	sfinv_buttons.register_button("sound_chat", {
		title = modSoundChat.translate("Chat"),
		action = function(player) edit_skin.show_formspec(player) end,
		tooltip = modSoundChat.translate("Open configuration of sound chat."),
		image = "icon_soundchat.png",
	})
elseif minetest.global_exists("sfinv") then
	sfinv.register_page("sound_chat", {
		title = modSoundChat.translate("Chat"),
		get = function(self, player, context) return "" end,
		on_enter = function(self, player, context)
			sfinv.contexts[player:get_player_name()].page = sfinv.get_homepage_name(player) --Se tirar essa linha bugará o formulário integrado 
			modSoundChat.showMainFormspec(player:get_player_name())
		end
	})
end
--------------------------------------------------------------------------------------------------------------

if minetest.global_exists("i3") then --OK
	i3.new_tab("sound_chat", {
		description = modSoundChat.translate("Chat Config"),
		--image = "icon_soundchat.png", -- Icon covers label
		access = function(player, data) return true end,

		formspec = function(player, data, fs) end,

		fields = function(player, data, fields)
			i3.set_tab(player, "inventory") --Indicará qual será o formuário padrão ao pressionar a tecla de inventário
			modSoundChat.showMainFormspec(player:get_player_name())
		end,
	})
end

--------------------------------------------------------------------------------------------------------------

if minetest.global_exists("unified_inventory") then
	unified_inventory.register_button("sound_chat", {
		type = "image",
		image = "icon_soundchat.png", --soundchat/textures/icon_soundchat.png
		tooltip = modSoundChat.translate("Chat Config"),
		action = function(player)
			modSoundChat.showMainFormspec(player:get_player_name())
		end,
	})
end

--------------------------------------------------------------------------------------------------------------

--[[  
if minetest.global_exists("i3") then
		i3.new_tab("edit_skin", {
			description = S("Edit Skin"),
			--image = "edit_skin_button.png", -- Icon covers label
			access = function(player, data) return true end,
	
			formspec = function(player, data, fs) end,

			fields = function(player, data, fields)
				i3.set_tab(player, "inventory")
				edit_skin.show_formspec(player)
			end,
		})
	end
	if minetest.global_exists("sfinv_buttons") then
		sfinv_buttons.register_button("edit_skin", {
			title = S("Edit Skin"),
			action = function(player) edit_skin.show_formspec(player) end,
			tooltip = S("Open skin configuration screen."),
			image = "edit_skin_button.png",
		})
	elseif minetest.global_exists("sfinv") then
		sfinv.register_page("edit_skin", {
			title = S("Edit Skin"),
			get = function(self, player, context) return "" end,
			on_enter = function(self, player, context)
				sfinv.contexts[player:get_player_name()].page = sfinv.get_homepage_name(player)
				edit_skin.show_formspec(player)
			end
		})
	end
	if minetest.global_exists("unified_inventory") then
		unified_inventory.register_button("edit_skin", {
			type = "image",
			image = "edit_skin_button.png",
			tooltip = S("Edit Skin"),
			action = function(player)
				edit_skin.show_formspec(player)
			end,
		})
	end
	if minetest.global_exists("armor") and armor.get_player_skin then
		armor.get_player_skin = function(armor, name)
			return edit_skin.compile_skin(edit_skin.player_skins[minetest.get_player_by_name(name)])
		end
	end
	if minetest.global_exists("inventory_plus") then
		minetest.register_on_player_receive_fields(function(player, formname, fields)
			if formname == "" and fields.edit_skin then
				edit_skin.show_formspec(player)
				return true
			end
			return false
		end)
	end
	if minetest.global_exists("smart_inventory") then
		smart_inventory.register_page({
			name = "skin_edit",
			icon = "edit_skin_button.png",
			tooltip = S("Edit Skin"),
			smartfs_callback = function(state) return end,
			sequence = 100,
			on_button_click  = function(state)
				local player = minetest.get_player_by_name(state.location.rootState.location.player)
				edit_skin.show_formspec(player)
			end,
			is_visible_func  = function(state) return true end,
        })
	end
--]]
