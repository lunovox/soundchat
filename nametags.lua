-- Chat Tags Settings

modSoundChat.ChatTags = {
   ["Video Publisher"] = {-- Nametags in chat
      priv = "videopublisher", --One word in lower case
      color = "#FFFF00", --Yellow Collor Code
      description = "Publisher of video about Minetest.",
   },
   ["Youtuber"] = {-- Nametags in chat
      priv = "youtuber", --One word in lower case
      color = "#FFFF00", --Yellow Collor Code
      description = "Publisher of video in Youtube about Minetest.",
   },
   ["Peertuber"] = {-- Nametags in chat
      priv = "peertuber", --One word in lower case
      color = "#FFFF00", --Yellow Collor Code
      description = "Publisher of video in Peertube about Minetest.",
   },
   ["Developer"] = {-- Nametags in chat
      priv = "developer", --One word in lower case
      color = "#00FFFF", --Cyan Collor Code
      description = "Can develop mods for Minetest and shutdown the server.",
   },
   ["Delegate"] = {-- Nametags in chat
      priv = "delegate", --One word in lower case
      color = "#6600FF", --Cyan Collor Code
      description = "Can ban and kick players to out of server if they break the rules.",
   },
   ["Medice"] = {-- Nametags in chat
      priv = "medice", --One word in lower case
      color = "#7F9DE3", --UTI Collor Code
      description = "Can revive players who had over permadeath bug.",
   },
   ["Mechanic"] = {-- Nametags in chat
      priv = "vehicle_mechanic", --One word in lower case
      color = "#FF8800", --orange
      description = "Allows you to repair and paint vehicles!",
   },
   ["Moderator"] = {-- Nametags in chat
      priv = "moderator", --One word in lower case
      color = "#FF9800", --Cyan Collor Code
      description = "You can listen to player complaints and report them to the delegate.",
   },
   ["Mayor"] = {-- Nametags in chat
      priv = "mayor", --One word in lower case
      color = "#FF00FF", --Cyan Collor Code
      description = "Can open chests and locked doors, can access town hall bank account, can give players paid jobs.",
   },
}
