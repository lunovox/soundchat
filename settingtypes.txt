# SOUNDCHAT SETTINGS

# ENABLED SOUNDCHAT:
# Enables all sounds of mod soundchat.
# Default: true
soundchat (Enabled Soundchat) bool true

# ATTENTION OF ALL PLAYERS:
# Enables sound in chat when the player sends a message to all players.
# Default: true
soundchat.call.onsendmessage (Call in Send Message) bool true

# ATTENTION OF ONE PLAYER:
# Play a alarm sound when send the player name in chat
# Default: true
soundchat.call.onplayername (Call in Player Name) bool true

# TERMINAL DIALOGS:
# Print in terminal all sended player dialogs.
# Default: true
soundchat.terminaldialogs (Player Dialogs in Terminal) bool true

# SAVE DIALOGS:
# Save all sended player dialogs (need mod lib_savelogs).
# Default: true
soundchat.savedialogs (Save Player Dialogs [need mod lib_savelogs]) bool true

# MAXIMUM DIALOGUE DISTANCE:
# Maximum distance someone has heard your typed message.
# 48 = 3 Chunks of 16 ditance blocks each.
# Default: 48 | Infinito: 0
soundchat.max_dialog_dist (Max Dialogue Distance) int 48 0 30000

# COLOR NAME PLAYER:
# Print the player name colored in terminal dialogs.
# Default: #00FF00
soundchat.color.player (Color Player Name) string #00FF00

# COLOR NAME ADMIN:
# Print the admin name colored in terminal dialogs.
# Default: #FF0000
soundchat.color.admin (Color Admin Name) string #FF0000

# SHOW SOUNDCHAT DEBUG:
# Allows you to print the debug information of this mod on the screen.
# Default: false
soundchat.debug (Show Soundchat Debug) bool false
