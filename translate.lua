local ngettext

--[[
local S = minetest.get_translator('testmod')
minetest.register_craftitem('testmod:test', {
    description = S('I am a test object'),
    inventory_image = 'default_stick.png^[brighten'
})
--]]

if minetest.get_modpath("intllib") then
	if intllib.make_gettext_pair then
		-- New method using gettext.
		modSoundChat.translate, ngettext = intllib.make_gettext_pair()
	else
		-- Old method using text files.
		modSoundChat.translate = intllib.Getter()
	end
elseif minetest.get_translator ~= nil and minetest.get_current_modname ~= nil and minetest.get_modpath(minetest.get_current_modname()) then
	modSoundChat.translate = minetest.get_translator(minetest.get_current_modname())
else
	modSoundChat.translate = function(s) return s end
end

modSoundChat.stripTagTranslate = function(text)
   for i=1, 9, 1 do
      text = text:gsub(string.byte(i).."(T@soundchat)", "")
      text = text:gsub(string.byte(i).."E", "")
   end
   text = minetest.strip_foreground_colors(text)
   text = minetest.strip_background_colors(text)
   text = minetest.strip_colors(text)
   return text
end